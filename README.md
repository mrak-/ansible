Задание по ansible

CentOS - https://hub.docker.com/r/centos/systemd/
Ubuntu - https://hub.docker.com/r/phusion/baseimage/

Поднять базу данных на основе образа убунты. Выбор MySQL/PostgreSQL/Mongo/etc.. (должен запускаться демоном)
Поднять два вебсервера на основе образов убунты и центоса. (apache - nginx) На вебсерверах должен быть скрипт, который который забирает из базы какое-то значение и показывает на странице.Так же скрипт должен показывать в каком контейнере он выполняется. Код скрипта на PHP/Python/Perl
Поднять контейнер на основе образа убунты для прокси сервера на Nginx/Haproxy для лоад балансинга. 

Порядок работы (всё делаем с помощю Ansible):
1. Поднять контейнеры и сформировать инвентори.
2. Установить и настроить сервер БД.
3. Установить и настроить выбранный веб-сервер со скриптом.
4. Установить и настроить выбранный прокси сервер.

Вся секретная информация должна быть зашифрованна.
Все серверы должны устанавливаться как сервисы.
Всё должно быть сделано с использованием модулей.
Прокси сервер должен обеспечивать проверку вебсерверов и переставать проксировать на контейнер в случае его падения.


Steps for setup:
1. Python-pip
 sudo apt-get install python-pip

2 install ansible
sudo pip install ansible

3. Create envirment for applications

ansible-playbook build_env.yml

4. Install services

ansible-playbook install_services.yml
